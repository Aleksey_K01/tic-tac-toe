package com.kn.kav.nullkrst;

public class WinnerCheckerHorizontal implements WinnerCheckerInterface
{
    private Game game;

    WinnerCheckerHorizontal(Game game) {
        this.game = game;
    }

    public Player checkWinner()
    {
        Square[][] field = game.getField();
        Player currPlayer;
        Player lastPlayer;
        for (Square[] aField : field) {
            lastPlayer = null;
            int successCounter = 1;
            for (int j = 0, len2 = aField.length; j < len2; j++) {
                currPlayer = aField[j].getPlayer();
                if (currPlayer == lastPlayer && currPlayer != null) {
                    successCounter++;
                    if (successCounter == len2) {
                        return currPlayer;
                    }
                }
                lastPlayer = currPlayer;
            }
        }
        return null;
    }
}