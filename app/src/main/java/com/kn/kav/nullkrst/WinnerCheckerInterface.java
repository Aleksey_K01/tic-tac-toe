package com.kn.kav.nullkrst;

public interface WinnerCheckerInterface {
    Player checkWinner();
}
