package com.kn.kav.nullkrst;
/*
Класс Player, объекты которого заполняют ячейки игрового поля.
 */
public class Player
{
    private String name;

    Player(String name)
    {
        this.name = name;
    }

    public CharSequence getName()
    {
        return name;
    }
}