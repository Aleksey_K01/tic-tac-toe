Игра Крестики-нолики

Описание : Игроки по очереди ставят на свободные клетки поля 3х3 знаки (один всегда крестики, другой всегда нолики). Первый, выстроивший в ряд 3 своих фигуры по вертикали, горизонтали или диагонали, выигрывает. 
Интерфейс: Русский ,английский.
Системные требования :OS Android 4+ .

При запуске игры появляется экран с запущенной игрой , от пользователя требуется поставить крестик или нолик в свободную клетку. При выигрыше отображается сообщение о победе крестика или нолика.